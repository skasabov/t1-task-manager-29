package ru.t1.skasabov.tm.api.service;

import ru.t1.skasabov.tm.api.repository.IUserOwnedRepository;
import ru.t1.skasabov.tm.model.AbstractUserOwnedModel;

public interface IUserOwnedService<M extends AbstractUserOwnedModel> extends IUserOwnedRepository<M> {

}
