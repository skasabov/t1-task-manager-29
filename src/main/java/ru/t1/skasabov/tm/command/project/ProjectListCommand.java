package ru.t1.skasabov.tm.command.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.skasabov.tm.enumerated.ProjectSort;
import ru.t1.skasabov.tm.model.Project;
import ru.t1.skasabov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

@NoArgsConstructor
public final class ProjectListCommand extends AbstractProjectCommand {

    @NotNull private static final String NAME = "project-list";

    @NotNull private static final String DESCRIPTION = "Show all projects.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[PROJECT LIST]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(ProjectSort.values()));
        @NotNull final String sortType = TerminalUtil.nextLine();
        @Nullable final ProjectSort sort = ProjectSort.toSort(sortType);
        @NotNull final String userId = getUserId();
        final boolean checkSort = sort == null;
        @Nullable final Comparator<Project> comparator = (checkSort ? null : sort.getComparator());
        @NotNull final List<Project> projects = getProjectService().findAll(userId, comparator);
        renderProjects(projects);
    }

}
