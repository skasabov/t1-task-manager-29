package ru.t1.skasabov.tm.command.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

@NoArgsConstructor
public final class TaskClearCommand extends AbstractTaskCommand {

    @NotNull private static final String NAME = "task-clear";

    @NotNull private static final String DESCRIPTION = "Remove all tasks.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[TASKS CLEAR]");
        @NotNull final String userId = getUserId();
        getTaskService().removeAll(userId);
    }

}
