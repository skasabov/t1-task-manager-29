package ru.t1.skasabov.tm.command.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.t1.skasabov.tm.model.Task;
import ru.t1.skasabov.tm.util.TerminalUtil;

import java.util.List;

@NoArgsConstructor
public final class TaskListByProjectIdCommand extends AbstractTaskCommand {

    @NotNull private static final String NAME = "task-show-by-project-id";

    @NotNull private static final String DESCRIPTION = "Show task list by project id.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[TASK LIST BY PROJECT ID]");
        System.out.println("ENTER PROJECT ID:");
        @NotNull final String projectId = TerminalUtil.nextLine();
        @NotNull final String userId = getUserId();
        @NotNull final List<Task> tasks = getTaskService().findAllByProjectId(userId, projectId);
        renderTasks(tasks);
    }

}
